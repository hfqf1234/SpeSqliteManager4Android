/**
 * Automatically generated file. DO NOT MODIFY
 */
package cn.autorepairehelper.spesqlite;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "cn.autorepairehelper.spesqlite";
  public static final String BUILD_TYPE = "debug";
}
